package tests;

import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC004_MergeLead extends Annotations{
	@Test
	public void mergeLead() {
		new MyHomePage()
		.clickLeadsTab()
		.clickMergeLead()
		.fromLeadImage()
		.selectFromLead()
		.toLeadImage()
		.selectToLead()
		.clickMergeButton();
		
		

}
}

