package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC002_DeleteLead extends Annotations {
	
	@BeforeClass
	public void setData() {
		excelFileName = "TC001";
	}
	@Test(dataProvider="fetchData")
	public void deleteLead(String cName, String fName, String lName) {
		new MyHomePage()
		.clickLeadsTab()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName()
		.findLeadtab()
		.enterLeadID()
		.clickFindLeads()
		.clickLeadID()
		.clickDelete();
		
		
		
	}
}
	
