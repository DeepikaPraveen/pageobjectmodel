package pages;

import org.openqa.selenium.Alert;

import libraries.Annotations;

public class MergeLeadPage extends Annotations  {
	
	public FindLeadsPage fromLeadImage() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		return new FindLeadsPage();
	}
	public FindLeadsPage toLeadImage() {
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		return new FindLeadsPage();
	}
	public ViewLeadPage clickMergeButton() {
		driver.findElementByLinkText("Merge").click();
		//switch to alert
		Alert alert = driver.switchTo().alert();
       alert.accept();
       return new ViewLeadPage();
	}
	

}
