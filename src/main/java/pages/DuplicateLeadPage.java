package pages;

import libraries.Annotations;

//import org.testng.annotations.BeforeClass;

public class DuplicateLeadPage extends Annotations {
	public DuplicateLeadPage  editLastname() {
		driver.findElementById("createLeadForm_lastName").sendKeys("K");
		return this;
	}
	public ViewLeadPage clickCreateLead() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}

}
