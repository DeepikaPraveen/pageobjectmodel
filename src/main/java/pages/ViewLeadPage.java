package pages;

import org.openqa.selenium.WebElement;

import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	
	public FindLeadsPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		/*if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}*/
		String leadid = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(leadid);
		 replaceAll = leadid.replaceAll("\\D","");
		 
		 
		return new FindLeadsPage();
	}
	public MyLeadsPage clickDelete() {
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
public DuplicateLeadPage clickDuplicateLead() {
	driver.findElementByLinkText("Duplicate Lead").click();
	return new DuplicateLeadPage();
}
	
	
	
	
	
	
}
