package pages;

import libraries.Annotations;

public class FindLeadsPage extends Annotations {
	public FindLeadsPage findLeadtab() {
		driver.findElementByLinkText("Find Leads").click();
		return this;
	}
	
	public FindLeadsPage enterLeadID() {
		
		driver.findElementByXPath("(//label[text()='Lead ID:']/following::input)[1]").sendKeys(replaceAll);
		return this;

}
	public FindLeadsPage clickFindLeads() {
		driver.findElementByLinkText("Find Leads");
		return  this;
	}
	
	public ViewLeadPage clickLeadID() {
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		return new ViewLeadPage();
	}
	
	public MergeLeadPage selectFromLead() {
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		return new MergeLeadPage();
	}
	public MergeLeadPage selectToLead() {
		driver.findElementByXPath("(//a[@class='linktext'])[5]").click();
		return new MergeLeadPage();
		
	}
}

